# Pod::Spec.new do |s|
#     s.name             = "BCPFrameworkManager"
#     s.version          = "1.0.0"
#     s.summary          = "Private repo to test."
#     s.homepage         = "https://bitbucket.org/dars1903/bcpframeworkmanager/src/master/"
#     s.license          = 'MIT'
#     s.author           = { "Daniel" => "dars1903@gmail.com" }
#     s.source           = { :git => "https://dars1903@bitbucket.org/dars1903/bcpframeworkmanager.git", :tag => s.version }
  
#     s.platform     = :ios, '9.0'
#     s.dependency 'Alamofire', '4.0.0'

#   end

Pod::Spec.new do |s|
  s.name = 'Alamofire'
  s.version = '5.0.5'
  s.license = 'MIT'
  s.summary = 'Elegant HTTP Networking in Swift'
  s.homepage = 'https://github.com/Alamofire/Alamofire'
  s.authors = { 'Alamofire Software Foundation' => 'info@alamofire.org' }
  s.source = { :git => 'https://github.com/Alamofire/Alamofire.git', :tag => s.version }
  s.documentation_url = 'https://alamofire.github.io/Alamofire/'

  s.ios.deployment_target = '10.0'
  s.osx.deployment_target = '10.12'
  s.tvos.deployment_target = '10.0'
  s.watchos.deployment_target = '3.0'

  s.swift_versions = ['5.0', '5.1']

  s.source_files = 'Source/*.swift'

  s.frameworks = 'CFNetwork'
end