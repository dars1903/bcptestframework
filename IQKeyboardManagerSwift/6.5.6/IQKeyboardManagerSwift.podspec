Pod::Spec.new do |s|
  s.name = 'IQKeyboardManagerSwift'
  s.version = '6.5.6'
  s.license = 'MIT'
  s.summary = 'Codeless drop-in universal library allows to prevent issues of keyboard sliding up and cover UITextField/UITextView.'
  s.homepage = 'https://github.com/hackiftekhar/IQKeyboardManager'
  s.authors = { 'Iftekhar Qurashi' => 'hack.iftekhar@gmail.com' }
  s.source = { :git => 'https://github.com/hackiftekhar/IQKeyboardManager.git', :tag => 'v6.5.6' }

  s.swift_versions = ['3.0', '3.2', '4.0', '4.2', '5.0', '5.1']

  s.source_files = 'IQKeyboardManagerSwift/**/*.{swift}'

  s.frameworks = 'UIKit'
  s.frameworks = 'Foundation'
  s.frameworks = 'CoreGraphics'
  s.frameworks = 'QuartzCore'

  s.requires_arc = true
  s.platform     = :ios, '8.0'
  
end